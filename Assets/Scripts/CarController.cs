using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class CarController : MonoBehaviour

{
    [SerializeField] float _acceleration = 300f, _turnRadius = 30f, _breakingForce = 10f;
    [SerializeField] Rigidbody _rigidbody;
    [SerializeField] WheelCollider[] _wheelColliders;
    [SerializeField] Transform[] _wheelTransforms;
    [SerializeField] Vector3 _centerOfMassCorrection;

    PlayerInput _playerInput;
    Transform _transform;

    float _currentMotorTorque, _currentSteerAngle, _currentBreakForce;
    
    bool IsAccelerating 
    {
        get
        {
            var accelerate = _playerInput.actions["accelerate"];
            return accelerate.IsPressed();
        }    
    }

    bool IsBraking
    {
        get
        {
            var brake = _playerInput.actions["brake"];
            return brake.IsPressed();
        }
    }
    
    float TurnAmount
    {
        get
        {
            var turn = _playerInput.actions["turn"];
            return turn.ReadValue<float>();
        }
    }

    void Start ()
    {
        _playerInput = GetComponent<PlayerInput>();
        _transform = transform;
        _rigidbody.centerOfMass = _centerOfMassCorrection;
    }

    void Update()
    {
        _currentMotorTorque = IsAccelerating ? _acceleration : 0f;
        _currentSteerAngle = TurnAmount * _turnRadius;
        _currentBreakForce = IsBraking ? _breakingForce : 0f;
    }

    void FixedUpdate ()
    {
        ApplyTurn();
        ApplyAcceleration();
        ApplyBraking();
    }

    void LateUpdate()
    {
        UpdateWheelTransforms();
    }

    void ApplyTurn()
    {
        for (var i = 0; i < 2; ++i)
        {
            Debug.Log($"Turn Amount: {_currentSteerAngle}");
            _wheelColliders[i].steerAngle =_currentSteerAngle;
        }
    }

    void ApplyAcceleration()
    {
        for (var i = 2; i < 4; ++i)
        {
            _wheelColliders[i].motorTorque = _currentMotorTorque;
        }
    }

    void ApplyBraking()
    {
        Debug.Log($"Apply Braking: IsBreaking: {IsBraking} {_currentBreakForce}");
        foreach (var wheelCollider in _wheelColliders)
        {
            wheelCollider.brakeTorque = _currentBreakForce;
        }
    }

    void UpdateWheelTransforms()
    {
        for (var i = 0; i < 4; ++i)
        {
            _wheelColliders[i].GetWorldPose(out var position, out var rotation);
            _wheelTransforms[i].position = position;
            _wheelTransforms[i].rotation = rotation;
        }
    }

    // called when we press down the accelerate input
    public void OnAccelerate ()
    {
    }

    // called when we modify the turn input
    public void OnTurn(InputValue inputValue)
    {
    }

    public void OnBrake()
    {
        
    }
}