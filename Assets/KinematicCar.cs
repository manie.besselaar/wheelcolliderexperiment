using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class KinematicCar : MonoBehaviour
{

    [SerializeField][Tooltip("How quickly the accelerates")] float _moveSpeed = 50;
    [SerializeField][Tooltip("Approximate max velocity for car")] float _maxSpeed = 50;
    [SerializeField][Tooltip("How much of the car velocity is preserved each update ")] float _drag = .98f;
    [SerializeField][] float _steerigLockAngle = 1;
    [SerializeField] float _steeringRate = 1;
    [SerializeField] Transform[] _frontWheels; 
    private float currentSteeringAngle;
    private Vector3 _moveForce;
    float _steeringInput;
    [SerializeField]float _traction = 1;
    [SerializeField] TrailRenderer[] _skidRenderers;
    [SerializeField][Tooltip("Higher values lead to more skidmarks. Values of 1 or above lead to constant skid marks")] float _skidvalue = 0.9f;
    [SerializeField] AnimationCurve _steeringCurve; // Used to change steering max lock as speed changes. Use same for movespeed over time?
   [SerializeField] float _effectiveSteeringLock;
    private void Start()
    {
        
        
       
        _effectiveSteeringLock = _steerigLockAngle;
    }

    // Update is called once per frame
    void Update()
    {
        _moveForce += transform.forward * _moveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
        transform.position += _moveForce * Time.deltaTime;

        _moveForce *= _drag;

        _moveForce = Vector3.ClampMagnitude(_moveForce, _maxSpeed);

        

        _moveForce = Vector3.Lerp(_moveForce.normalized, transform.forward, _traction * Time.deltaTime) * _moveForce.magnitude;
        if (_moveForce.magnitude > .1f)
        {
           _effectiveSteeringLock = _steeringCurve.Evaluate(_moveForce.magnitude / _maxSpeed) * _steerigLockAngle;
            float result =  _moveForce.magnitude /_maxSpeed;
            Debug.Log("Steering curve " + _steeringCurve.Evaluate(result) + " at " + result);
            //Change this to smooth out input


            //
            //  Debug.Log("Steering lock " + _effectiveSteeringLock + " moveForce " + _moveForce.magnitude);

            _steeringInput = Mathf.Lerp(_steeringInput, Input.GetAxis("Horizontal"), Time.deltaTime * _steeringRate);
            transform.Rotate(Vector3.up * _steeringInput * _effectiveSteeringLock * Mathf.Rad2Deg * Time.deltaTime);
            Debug.Log("Steering result " + (_steeringInput * _effectiveSteeringLock * Mathf.Rad2Deg * Time.deltaTime).ToString());

        }
        

        SteerWheels(_steeringInput * _effectiveSteeringLock);
      

        CheckSkid();
    }

    private void SteerWheels(float steeringAngle)
    {
        foreach(Transform wheel in _frontWheels)
        {
           // Debug.Log("steering " + steeringAngle);
           //TODO: adjust this to also conform the the steering curve 
            wheel.localRotation = Quaternion.Euler(0, steeringAngle, 0);
        }
    }

    private void CheckSkid()
    {
        bool skidStatus = Vector3.Dot(transform.forward, _moveForce.normalized) < .9f;


       foreach (TrailRenderer skidLine in _skidRenderers)
        {
            skidLine.emitting= skidStatus;

            // Add particle systems for the smoke and start stop them here. Remember to set coordinate system to world so that the smoke does not turn and follow
            // the car, but hangs where it was emitted.
        }
    }
}
