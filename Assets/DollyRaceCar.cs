using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollyRaceCar : MonoBehaviour
{

   // [SerializeField] Transform _parent;
    [SerializeField] float _offsetLimits = 10;
    [SerializeField]float _slipFactor = 10;
    [SerializeField][Tooltip("Speed for the lateral movement of the car.")] float _steerSpeed = .5f;
    [SerializeField] float _maxSpeed = 40;
    [SerializeField] float _acceleration = 3;
    [SerializeField] float _deceleration = 8;
    [SerializeField] float _minSpeed = 10;
    CinemachinePath _path;
  [SerializeField]  CinemachineDollyCart _cart;
    Vector3 _previousPosition;
    Vector3 _velocityVector;
    float _xVariance;
    RaycastHit _hit;
   [SerializeField] Color _castColor;
    [SerializeField] Color _rumbleStripColor;
   [SerializeField] float _colorTolerance;
    [SerializeField] Speedo _speedo;
    // Start is called before the first frame update
    void Start()
    {
        _cart.m_Speed = _maxSpeed;
        _previousPosition = transform.position;
        StartCoroutine(UpdateSpeedo());
    }

  IEnumerator UpdateSpeedo()
    {
        while (gameObject.active)
        {
            yield return new WaitForSeconds(.3f);
            _speedo.UpdateSpeedo(_velocityVector.magnitude * _maxSpeed, _maxSpeed);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void LateUpdate()
    {
        //Doing all the calculations in LateUpdate because the dolly that follows the path moves the car during update and I don't 
        //want it to override what I am doing here. May be unnecesary since I am changing the local position and the dolly is
        //moving the parent. Needs more investigation
        //Figure out where the car would go without intervention
        _velocityVector =transform.InverseTransformPoint( transform.position) - transform.InverseTransformPoint( _previousPosition);

       
       
        //Move the local position laterally taking into account the velocity vector and the steering input
        _xVariance =( _slipFactor * _velocityVector.x)+ (Input.GetAxis("Horizontal") * _steerSpeed * Time.fixedDeltaTime);
        _xVariance =Mathf.Clamp( transform.localPosition.x + _xVariance ,-_offsetLimits,_offsetLimits); // Stay within the set limits to avoid going too far off track
        transform.localPosition =  new Vector3(_xVariance,transform.localPosition.y,transform.localPosition.z);
      
        //Make this position the _previous position for the next velocity calculation
        _previousPosition = transform.position;
      //  CheckRoad();
        CheckRoadWithExtents(_xVariance);

    }
    /// <summary>
    /// Check how far off from the laid out path the player is . If too far off then slow the car down.
    /// Easy but can be imprecice if the path is not precisely centered on the road at all positions.
    /// </summary>
    /// <param name="xVariance"></param>
    private void CheckRoadWithExtents(float xVariance)
    {
        if(MathF.Abs(xVariance) > _offsetLimits * .8f)
        {
            Debug.DrawRay(transform.position, Vector3.down, Color.red,1);
            _cart.m_Speed = Mathf.Clamp(_cart.m_Speed - _deceleration * Time.fixedDeltaTime, _minSpeed, _maxSpeed);
           
        }
        else
        {
            Debug.DrawRay(transform.position, Vector3.down, Color.blue, 1);
            _cart.m_Speed = Mathf.Clamp(_cart.m_Speed + _acceleration * Time.fixedDeltaTime, _minSpeed, _maxSpeed);
        }
    }

    void CheckRoad()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position,Vector3.down, out hit))
            return;
      

        Renderer rend = hit.transform.GetComponent<Renderer>();
      //  MeshCollider meshCollider = hit.collider as MeshCollider;
       
        /*
         When raycasting I can detect the white and green of the track segments but not the road color. Not sure why. Must have something
        to do with how the road segment models are set up.

        If I put a cube under the car the raycast returns the expected road color.
        Need to as for help here.
        Material detection is onconsistent. Randomly switched between materials on the road models. Random cube with single material works well.
        Still need som work.
         */
        
        _castColor = rend.material.color;
   
        Debug.Log("material " + rend.material.name );

        if (string.Compare(rend.material.name,"grey") ==1) 
        {
            _cart.m_Speed =Mathf.Clamp( _cart.m_Speed - _deceleration * Time.fixedDeltaTime,_minSpeed,_maxSpeed) ;
            Debug.DrawLine(transform.position, hit.point, Color.red, 1);

        }
        else
        {
            Debug.DrawLine(transform.position, hit.point, Color.blue, 1);
            _cart.m_Speed = Mathf.Clamp(_cart.m_Speed + _acceleration * Time.fixedDeltaTime, _minSpeed, _maxSpeed);
        }
    }
}

