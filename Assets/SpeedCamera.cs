using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedCamera : MonoBehaviour
{
    [SerializeField] Cinemachine.CinemachineVirtualCamera _camera;
    [SerializeField] float _focalLengthChangeRatio = 0.02f;
    float _defaultFocalLength;
   [SerializeField] Rigidbody _trackedRigidBody;

    private void Awake()
    {
        _defaultFocalLength = _camera.m_Lens.FieldOfView;
    }
    private void FixedUpdate()
    {
        _camera.m_Lens.FieldOfView = _defaultFocalLength - (_focalLengthChangeRatio * _trackedRigidBody.velocity.magnitude);
    }
}
