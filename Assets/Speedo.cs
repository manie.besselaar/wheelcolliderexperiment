using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Speedo : MonoBehaviour
{
    [SerializeField] Rigidbody _measuredBody;
    [SerializeField] Image _speedoImage;
    [SerializeField] TextMeshProUGUI _speedoText;
    [SerializeField] TextMeshProUGUI _topSpeedText;
    int _topSpeed = 0;
    int _currentSpeed = 0;    
    // Start is called before the first frame update


    // Update is called once per frame
    private void FixedUpdate()
    {
       
    }
    public void UpdateSpeedo(float speed, float maxSpeed)
    {
        _speedoImage.fillAmount = speed/maxSpeed;
       

        _speedoText.text = ((int)speed).ToString();
        if (speed > _topSpeed)
        {
            _topSpeed =(int) speed;
            _topSpeedText.text = _topSpeed.ToString();
        }
    }
}
